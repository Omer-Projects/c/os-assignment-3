#!/usr/bin/bash

# first check that the "stnc" server running!

sleep 1
./stnc -c 127.0.0.1 8000 -p ipv4 tcp -q
sleep 0.25
./stnc -c 127.0.0.1 8000 -p ipv4 udp -q
sleep 0.25
./stnc -c 127.0.0.1 8000 -p ipv6 tcp -q
sleep 0.25
./stnc -c 127.0.0.1 8000 -p ipv6 udp -q
sleep 0.25
./stnc -c 127.0.0.1 8000 -p uds dgram -q
sleep 0.25
./stnc -c 127.0.0.1 8000 -p uds stream -q
sleep 0.25
./stnc -c 127.0.0.1 8000 -p mmap mmap-file.txt -q
sleep 0.25
./stnc -c 127.0.0.1 8000 -p pipe pipe-file.pipe -q

sleep 0.25
./stnc -c 127.0.0.1 8000 -p ipv4 tcp
sleep 0.25
./stnc -c 127.0.0.1 8000 -p ipv4 udp
sleep 0.25
./stnc -c 127.0.0.1 8000 -p ipv6 tcp
sleep 0.25
./stnc -c 127.0.0.1 8000 -p ipv6 udp
sleep 0.25
./stnc -c 127.0.0.1 8000 -p uds dgram
sleep 0.25
./stnc -c 127.0.0.1 8000 -p uds stream
sleep 0.25
./stnc -c 127.0.0.1 8000 -p mmap mmap-file.txt
sleep 0.25
./stnc -c 127.0.0.1 8000 -p pipe pipe-file.pipe
