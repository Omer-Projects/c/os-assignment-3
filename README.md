# os-assignment-3

Assignment 3 for "Operating Systems"

* For more information, you can see in the PDF in the docs directory.

## Requirements

* make
* gcc

## Build

For install this tools run the following command in the terminal:

```bash
make all
```

Or if is already built

```bash
make rebuild
```

## Get Started

For open the server run the following command in the terminal:

```bash
./stnc -s 8000
```

If in the "chat" you right only the word "exit" in the server / client, it will close the chat. \
In addition, to CTRL+C

## Testing

Before testing the tool make share that the server is running.\
Now, you can run the following commands:

```bash
bash ./test.sh
./stnc -c 127.0.0.1 8000
```

## Customization

You can customize the app in src/shared.h (I know is not in the right way!).

## Authors

- Omer Priel

## License

MIT
