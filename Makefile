# Compiler
CC=gcc -Wall

# Globals-
SRC=src
BIN=bin

# CI/CD
clean:
	rm -rf $(BIN) stnc

clean-all: clean
	rm -rf uds-dgram.socket uds-steam.socket mmap-file.txt pipe-file.pipe

# dist folders
$(BIN):
	mkdir $(BIN)

# units
$(BIN)/shared.o: $(BIN) $(SRC)/shared.c $(SRC)/shared.h
	$(CC) -c "$(SRC)/shared.c" -o $(BIN)/shared.o

$(BIN)/handlers.o: $(BIN) $(SRC)/handlers.c $(SRC)/handlers.h
	$(CC) -c "$(SRC)/handlers.c" -o $(BIN)/handlers.o

$(BIN)/client.o: $(BIN) $(SRC)/client.c $(SRC)/client.h
	$(CC) -c "$(SRC)/client.c" -o $(BIN)/client.o

$(BIN)/server.o: $(BIN) $(SRC)/server.c $(SRC)/server.h
	$(CC) -c "$(SRC)/server.c" -o $(BIN)/server.o

# applications
stnc: $(BIN) $(SRC)/stnc.c $(BIN)/shared.o $(BIN)/handlers.o $(BIN)/client.o $(BIN)/server.o
	$(CC) -c "$(SRC)/stnc.c" -o $(BIN)/stnc.o
	$(CC) -o stnc $(BIN)/stnc.o $(BIN)/shared.o $(BIN)/handlers.o $(BIN)/client.o $(BIN)/server.o

# build
all: stnc

rebuild: clean all

# tests
test-server: rebuild
	./stnc -s 8000

test-server-mode-quit: rebuild
	./stnc -s 8000 -q

test-client-e2e:
	sleep 1
	./stnc -c 127.0.0.1 8000
	sleep 0.2
	./stnc -c 127.0.0.1 8000 -p ipv4 tcp

test-client-chat:
	sleep 1
	./stnc -c 127.0.0.1 8000

test-client-test-mode:
	sleep 5
	./stnc -c 127.0.0.1 8000 -p ipv4 tcp
	sleep 1
	./stnc -c 127.0.0.1 8000 -p ipv4 udp
	sleep 1
	./stnc -c 127.0.0.1 8000 -p ipv6 tcp
	sleep 1
	./stnc -c 127.0.0.1 8000 -p ipv6 udp
	sleep 1
	./stnc -c 127.0.0.1 8000 -p uds dgram
	sleep 1
	./stnc -c 127.0.0.1 8000 -p uds stream
	sleep 1
	./stnc -c 127.0.0.1 8000 -p mmap mmap-file.txt
	sleep 1
	./stnc -c 127.0.0.1 8000 -p pipe pipe-file.pipe

test-client-test-mode-quit:
	sleep 5
	./stnc -c 127.0.0.1 8000 -p ipv4 tcp -q
	sleep 0.25
	./stnc -c 127.0.0.1 8000 -p ipv4 udp -q
	sleep 0.25
	./stnc -c 127.0.0.1 8000 -p ipv6 tcp -q
	sleep 0.25
	./stnc -c 127.0.0.1 8000 -p ipv6 udp -q
	sleep 0.25
	./stnc -c 127.0.0.1 8000 -p uds dgram -q
	sleep 0.25
	./stnc -c 127.0.0.1 8000 -p uds stream -q
	sleep 0.25
	./stnc -c 127.0.0.1 8000 -p mmap mmap-file.txt -q
	sleep 0.25
	./stnc -c 127.0.0.1 8000 -p pipe pipe-file.pipe -q
