// shard objects

#ifndef SHARED_H
#define SHARED_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <netinet/tcp.h>
#include <sys/time.h>
#include <poll.h>
#include <time.h>
#include <sys/un.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

// max number of requests, is 0 - infinity
#define MAX_LOOPS 0

#define TEST_DATA_SIZE 100000000
#define TEST_DATA_FULL_SIZE (TEST_DATA_SIZE + 1)
#define TEST_BUFFER_SIZE 10000000
#define TEST_PORT 8001
#define TEST_UDS_DGRAM_SOCKET_PATH "./uds-dgram.socket"
#define TEST_UDS_STREAM_SOCKET_PATH "./uds-steam.socket"

#define BUFFER_SIZE 1024
#define BUFFER_DATA_SIZE (BUFFER_SIZE - 4)
#define POLL_TIMEOUT 1000

/**
 * @brief The tool options
 *
 */
typedef struct Options {
    unsigned char is_server;
    char* ip;
    unsigned short port;
    unsigned char command_type;
	char* test_param;
    unsigned char quiet;
} Options;

/**
 * wrapper for send. its add size of the data in the start of the data
 *
 * @param sock socket
 * @param buffer the full buffer (data size + data)
 * @param dataSize the size of the data in bytes
 * @return 0 - success, -1 - if has error
 */
int safe_send(int sock, char *buffer, int dataSize);

/**
 * wrapper for recv. its take the size of the data from the first bytes
 *
 * @param sock socket
 * @param buffer buffer for the data size and the data
 * @param dataSize the size of the data that need to come
 * @return 0 - success, -1 - if has error
 */
int safe_recv(int sock, char* buffer, int dataSize);

/**
 * calculate simple checksum
 *
 * @param start start of buffer
 * @param end end of buffer
 * @return the checksum
 */
char checksum(char* start, char* end);

/**
 * Print test result
 *
 * @param options options pointer
 * @param command_type of the test
 * @param start start time in ms
 * @param end end time in ms
 */
void print_result(Options* options, unsigned char command_type, long long start, long long end);

/**
 * gives the current time in milliseconds
 *
 * @return current time in milliseconds
 */
long long current_time_in_milliseconds();

#endif
