// shard objects

#ifndef HANDLERS_H
#define HANDLERS_H

#include "shared.h"

#define UDP_BUFFER_MAX_SIZE 1500

typedef void (*send_data)(Options options, char* data, int dataSize);
typedef void (*recv_data)(int clientSocket, char* socketBuffer, char* socketBufferData, char* data, int dataSize);

// handlers
void ipv4_tcp_send_data(Options options, char* data, int dataSize);
void ipv4_tcp_recv_data(int clientSocket, char* socketBuffer, char* socketBufferData, char* data, int dataSize);

void ipv4_udp_send_data(Options options, char* data, int dataSize);
void ipv4_udp_recv_data(int clientSocket, char* socketBuffer, char* socketBufferData, char* data, int dataSize);

void ipv6_tcp_send_data(Options options, char* data, int dataSize);
void ipv6_tcp_recv_data(int clientSocket, char* socketBuffer, char* socketBufferData, char* data, int dataSize);

void ipv6_udp_send_data(Options options, char* data, int dataSize);
void ipv6_udp_recv_data(int clientSocket, char* socketBuffer, char* socketBufferData, char* data, int dataSize);

void uds_dgram_send_data(Options options, char* data, int dataSize);
void uds_dgram_recv_data(int clientSocket, char* socketBuffer, char* socketBufferData, char* data, int dataSize);

void uds_stream_send_data(Options options, char* data, int dataSize);
void uds_stream_recv_data(int clientSocket, char* socketBuffer, char* socketBufferData, char* data, int dataSize);

void mmap_send_data(Options options, char* data, int dataSize);
void mmap_recv_data(int clientSocket, char* socketBuffer, char* socketBufferData, char* data, int dataSize);

void pipe_send_data(Options options, char* data, int dataSize);
void pipe_recv_data(int clientSocket, char* socketBuffer, char* socketBufferData, char* data, int dataSize);

#endif
