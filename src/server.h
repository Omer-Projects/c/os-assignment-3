// server.h

#ifndef SERVER_H
#define SERVER_H

#include "shared.h"

/**
 * run the server app
 *
 * @param options options
 * @return error code
 */
int server_app(Options options);

#endif
