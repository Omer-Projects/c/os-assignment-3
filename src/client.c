#include "client.h"
#include "handlers.h"

int handle_request(Options options, int serverSocket);
int handle_request_chat(Options options, int serverSocket, char* socketBuffer, char* socketBufferData);
int handle_request_test(Options options, int serverSocket, char* socketBuffer, char* socketBufferData);
char* generate_data();

int client_app(Options options)
{
    // Contenting to the server
    struct sockaddr_in serverAddress;

    // create a client socket
    int serverSocket = socket(AF_INET, SOCK_STREAM, 0);

    if (serverSocket == -1)
    {
        printf("ERROR: Could not create socket: %d\n", errno);
        return 1;
    }

    // config and save the server address
    bzero(&serverAddress, sizeof(serverAddress));
    serverAddress.sin_family = AF_INET;
    serverAddress.sin_port = htons(options.port);
    int rval = inet_pton(AF_INET, (const char *)options.ip, &serverAddress.sin_addr);
    if (rval <= 0)
    {
        printf("ERROR: inet_pton() failed\n");
        close(serverSocket);
        return 1;
    }

    // connect to the server
    if (connect(serverSocket, (struct sockaddr *)&serverAddress, sizeof(serverAddress)) == -1)
    {
        printf("ERROR: connect() failed with error code: %d\n", errno);
        close(serverSocket);
        return 1;
    }

    int errorCode = handle_request(options, serverSocket);
    close(serverSocket);

    return errorCode;
}

int handle_request(Options options, int serverSocket)
{
    char socketBuffer[BUFFER_SIZE];
    char *socketBufferData = (socketBuffer + sizeof(int));

    if (options.command_type == 1) {
        return handle_request_chat(options, serverSocket, socketBuffer, socketBufferData);
    }

    return handle_request_test(options, serverSocket, socketBuffer, socketBufferData);
}

int handle_request_chat(Options options, int serverSocket, char* socketBuffer, char* socketBufferData)
{
    *socketBufferData = (char)options.command_type;

    if (safe_send(serverSocket, socketBuffer, 1) < 0) {
        printf("ERROR: Network error!\n");
        return 1;
    }

    struct pollfd fds[2];
    fds[0].fd = STDIN_FILENO;
    fds[0].events = POLL_IN;
    fds[0].revents = 0;
    fds[1].fd = serverSocket;
    fds[1].events = POLL_IN;
    fds[1].revents = 0;

    while (1)
    {
        poll(fds, 2, POLL_TIMEOUT);

        // check if has text to send
        if (fds[0].revents & POLLIN)
        {
            char* ptr = socketBufferData;
            int i = 0;
            read(STDIN_FILENO, ptr, 1);
            while (*ptr != '\n')
            {
                i++;
                ptr++;
                read(STDIN_FILENO, ptr, 1);
            }

            *ptr = '\0';

            if (strcmp(socketBufferData, "exit") == 0) {
                return 0;
            }

            safe_send(serverSocket, socketBuffer, i);
        }

        // check text received
        if (fds[1].revents & POLLIN)
        {
            if (safe_recv(serverSocket, socketBuffer, BUFFER_DATA_SIZE) == -1)
            {
                return 0;
            }
            printf("%s\n", socketBufferData);
        }
    }
}

int handle_request_test(Options options, int serverSocket, char* socketBuffer, char* socketBufferData)
{
    send_data sendDataHandlers[8] = { &ipv4_tcp_send_data, &ipv4_udp_send_data,
                                      &ipv6_tcp_send_data, &ipv6_udp_send_data,
                                      &uds_dgram_send_data, &uds_stream_send_data,
                                      &mmap_send_data, &pipe_send_data };

    // generate the data
    char* data = generate_data();
    if (data == NULL) {
        return 1;
    }

    // create request packet
    int pocketLength = 1;
    *socketBufferData = (char)options.command_type;
    if (options.command_type == 8 || options.command_type == 9)
    {
        strcpy(socketBufferData + 1, options.test_param);
        pocketLength += (int)strlen(options.test_param) + 1;
    }

    // send request
    if (safe_send(serverSocket, socketBuffer, pocketLength) < 0) {
        printf("ERROR: Network error!\n");
        return 1;
    }

    // wait for lets start
    if (safe_recv(serverSocket, socketBuffer, 0) < 0)
    {
        printf("ERROR: Network error!\n");
        return 1;
    }

    long long start = current_time_in_milliseconds();

    // send the data
    (*sendDataHandlers[options.command_type - 2])(options, data, TEST_DATA_FULL_SIZE);

    *(long long*)socketBufferData = start;
    if (safe_send(serverSocket, socketBuffer, 8) < 0) {
        printf("ERROR: Network error!\n");
        return 1;
    }

    *(long long*)socketBufferData = 0;

    // wait for the result
    if (safe_recv(serverSocket, socketBuffer, sizeof(long long)) < 0)
    {
        printf("ERROR: Network error!\n");
        return 1;
    }

    long long end = *(long long*)socketBufferData;

    print_result(&options, options.command_type, start, end);

    return 0;
}

char* generate_data()
{
    srand(time(NULL));

    char* data = malloc(TEST_DATA_FULL_SIZE);
    if (data == NULL)
    {
        printf("Error: In malloc");
        return NULL;
    }
    char* ptr = data + 1;
    char* end = data + 1 + TEST_DATA_SIZE;
    while (ptr != end)
    {
        int number = rand() % 10;
        *ptr = (char)((int)'0' + number);
        ptr++;
    }

    *data = checksum(data + 1, data + 1 + TEST_DATA_SIZE);

    return data;
}
