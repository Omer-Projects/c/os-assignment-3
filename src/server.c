#include "server.h"
#include "handlers.h"

void handle_client(Options options, int clientSocket);
void handle_client_chat(Options options, int clientSocket, char* socketBuffer, char *socketBufferData);
void handle_client_test(Options options, int clientSocket, char* socketBuffer, char *socketBufferData);

int server_app(Options options)
{
    // create listening socket
    int listeningSocket = socket(AF_INET, SOCK_STREAM, 0);

    if (listeningSocket == -1)
    {
        printf("ERROR: Could not create listening socket: %d\n", errno);
        return 1;
    }


    int enableReuse = 1;
    if (setsockopt(listeningSocket, SOL_SOCKET, SO_REUSEADDR, &enableReuse, sizeof(int)) < 0)
    {
        printf("ERROR: setsockopt() failed with error code: %d\n", errno);
        close(listeningSocket);
        return 1;
    }

    struct sockaddr_in serverAddress;
    bzero((char*)&serverAddress, sizeof(serverAddress));

    serverAddress.sin_family = AF_INET;
    serverAddress.sin_addr.s_addr = INADDR_ANY;
    serverAddress.sin_port = htons(options.port);
    if (bind(listeningSocket, (struct sockaddr *)&serverAddress, sizeof(serverAddress)) == -1)
    {
        printf("ERROR: Bind failed with error code: %d\n", errno);
        close(listeningSocket);
        return 1;
    }

    // start to listen for accepts
    if (listen(listeningSocket, 1) == -1)
    {
        printf("ERROR: listen() failed with error code: %d\n", errno);
        close(listeningSocket);
        return 1;
    }

    struct sockaddr_in clientAddress;
    socklen_t clientAddressLen = sizeof(clientAddress);

    // the main loop
    int loop = 0;
    while (!MAX_LOOPS || loop < MAX_LOOPS)
    {
        loop++;

        // waiting for client accept
        if (!options.quiet)
        {
            printf("INFO: Waiting requests (request number: %d)\n", loop);
        }

        memset(&clientAddress, 0, sizeof(clientAddress));
        clientAddressLen = sizeof(clientAddress);
        int clientSocket = accept(listeningSocket, (struct sockaddr *) &clientAddress, &clientAddressLen);
        if (clientSocket == -1) {
            printf("ERROR: Listen failed with error code: %d\n", errno);
            close(clientSocket);
            continue;
        }

        handle_client(options, clientSocket);

        close(clientSocket);
    }

    close(listeningSocket);
    return 0;
}

void handle_client(Options options, int clientSocket)
{
    char socketBuffer[BUFFER_SIZE];
    char *socketBufferData = (socketBuffer + sizeof(int));

    safe_recv(clientSocket, socketBuffer, BUFFER_DATA_SIZE);

    unsigned char command_type = *(unsigned char*)socketBufferData;
    if (command_type == 1) {
        handle_client_chat(options, clientSocket, socketBuffer, socketBufferData);
    } else {
        handle_client_test(options, clientSocket, socketBuffer, socketBufferData);
    }
}

void handle_client_chat(Options options, int clientSocket, char* socketBuffer, char *socketBufferData) {
    struct pollfd fds[2];
    fds[0].fd = STDIN_FILENO;
    fds[0].events = POLL_IN;
    fds[0].revents = 0;
    fds[1].fd = clientSocket;
    fds[1].events = POLL_IN;
    fds[1].revents = 0;

    while (1) {
        poll(fds, 2, POLL_TIMEOUT);

        // check if has text to send
        if (fds[0].revents & POLLIN) {
            char* ptr = socketBufferData;
            int i = 0;
            read(STDIN_FILENO, ptr, 1);
            while (*ptr != '\n')
            {
                i++;
                ptr++;
                read(STDIN_FILENO, ptr, 1);
            }

            *ptr = '\0';

            if (strcmp(socketBufferData, "exit") == 0) {
                return;
            }

            if (safe_send(clientSocket, socketBuffer, i) < 0) {
                return;
            }
        }

        // check text received
        if (fds[1].revents & POLLIN) {
            if (safe_recv(clientSocket, socketBuffer, BUFFER_DATA_SIZE) == -1) {
                return;
            }

            printf("%s\n", socketBufferData);
        }
    }
}

void handle_client_test(Options options, int clientSocket, char* socketBuffer, char *socketBufferData)
{
    recv_data recvDataHandlers[8] = { &ipv4_tcp_recv_data, &ipv4_udp_recv_data,
                                      &ipv6_tcp_recv_data, &ipv6_udp_recv_data,
                                      &uds_dgram_recv_data, &uds_stream_recv_data,
                                      &mmap_recv_data, &pipe_recv_data };

    unsigned char command_type = *(unsigned char*)socketBufferData;

    // create container for the data
    char* data = malloc(TEST_DATA_FULL_SIZE);
    if (data == NULL)
    {
        printf("Error: In malloc");
        return;
    }

    *data = '\1';

    (*recvDataHandlers[command_type - 2])(clientSocket, socketBuffer, socketBufferData, data, TEST_DATA_FULL_SIZE);

	if (safe_recv(clientSocket, socketBuffer, 8) == -1) {
        return;
    }

    long long start = *(long long*)socketBufferData;

    if (*data != checksum(data + 1, data + 1 + TEST_DATA_SIZE))
    {
        // send the error result
        *(long long*)socketBufferData = 0;
        safe_send(clientSocket, socketBuffer, sizeof(long long));
        return;
    }

    // send the result
    long long end = current_time_in_milliseconds();
    *(long long*)socketBufferData = end;
    safe_send(clientSocket, socketBuffer, sizeof(long long));

    print_result(&options, command_type, start, end);
}
