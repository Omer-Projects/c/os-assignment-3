#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "shared.h"
#include "client.h"
#include "server.h"

/**
 * @brief Entry point of stnc
 *
 * @return int error code
 */
int main(int argc, char *argv[])
{
    Options options;
    options.is_server = 1;
    options.ip = NULL;
    options.port = 0;
    options.command_type = 1;
    options.test_param = NULL;
    options.quiet = 0;

    // load arguments
    for (size_t i = 1; i < argc; i++)
    {
        if (strcmp(argv[i], "-s") == 0) {
            if (i + 1 == argc) {
                printf("Missing PORT!\n");
                return 2;
            }

            options.is_server = 1;
            options.port = atoi(argv[i + 1]);
            i++;

        } else if (strcmp(argv[i], "-c") == 0) {
            if (i + 2 >= argc) {
                printf("Missing arguments!\n");
                return 2;
            }

            options.is_server = 0;
            options.ip = argv[i + 1];
            options.port = atoi(argv[i + 2]);
            i += 2;
        } else if (strcmp(argv[i], "-p") == 0) {
            if (i + 2 >= argc) {
                printf("Missing arguments!\n");
                return 2;
            }

            if (strcmp(argv[i + 1], "ipv4") == 0 && strcmp(argv[i + 2], "tcp") == 0) {
                options.command_type = 2;
            } else if (strcmp(argv[i + 1], "ipv4") == 0 && strcmp(argv[i + 2], "udp") == 0) {
                options.command_type = 3;
            } else if (strcmp(argv[i + 1], "ipv6") == 0 && strcmp(argv[i + 2], "tcp") == 0) {
                options.command_type = 4;
            } else if (strcmp(argv[i + 1], "ipv6") == 0 && strcmp(argv[i + 2], "udp") == 0) {
                options.command_type = 5;
            } else if (strcmp(argv[i + 1], "uds") == 0 && strcmp(argv[i + 2], "dgram") == 0) {
                options.command_type = 6;
            } else if (strcmp(argv[i + 1], "uds") == 0 && strcmp(argv[i + 2], "stream") == 0) {
                options.command_type = 7;
            } else if (strcmp(argv[i + 1], "mmap") == 0) {
                options.command_type = 8;
                options.test_param = argv[i + 2];
            } else if (strcmp(argv[i + 1], "pipe") == 0) {
                options.command_type = 9;
                options.test_param = argv[i + 2];
            } else {
                printf("Invalid arguments!\n");
                return 2;
            }
            i += 2;
        } else if (strcmp(argv[i], "-q") == 0) {
            options.quiet = 1;
        } else {
            printf("Invalid arguments!\n");
            return 2;
        }
    }

	if (options.port == 0) {
		printf("Missing arguments!\n");
        return 2;
	}

    // open the app
    if (options.is_server) {
        return server_app(options);
    }

    return client_app(options);
}
