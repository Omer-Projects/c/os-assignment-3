#include "handlers.h"

// ipv4_tcp
void ipv4_tcp_send_data(Options options, char* data, int dataSize)
{
    // open connection
    // create a client socket
    int sock = socket(AF_INET, SOCK_STREAM, 0);

    if (sock == -1)
    {
        printf("ERROR: Could not create socket: %d\n", errno);
        return;
    }

    // config and save the server address
    struct sockaddr_in serverAddress;

    bzero(&serverAddress, sizeof(serverAddress));
    serverAddress.sin_family = AF_INET;
    serverAddress.sin_port = htons(TEST_PORT);
    if (inet_pton(AF_INET, (const char *)options.ip, &serverAddress.sin_addr) <= 0)
    {
        printf("ERROR: inet_pton() failed\n");
        close(sock);
        return;
    }

    // connect to the server
    if (connect(sock, (struct sockaddr *)&serverAddress, sizeof(serverAddress)) == -1)
    {
        printf("ERROR: connect() failed with error code: %d\n", errno);
        close(sock);
        return;
    }

    // send data
    while (dataSize > 0)
    {
        int bytesSent = (int)send(sock, data, (dataSize < TEST_BUFFER_SIZE) ? dataSize : TEST_BUFFER_SIZE, 0);

        if (bytesSent > 0) {
            data += bytesSent;
            dataSize -= bytesSent;
        }
    }

    // close connection
    close(sock);
}

void ipv4_tcp_recv_data(int clientSocket, char* socketBuffer, char* socketBufferData, char* data, int dataSize)
{
    // open connection
    // create listening socket
    int listeningSocket = socket(AF_INET, SOCK_STREAM, 0);

    if (listeningSocket == -1)
    {
        printf("ERROR: Could not create listening socket: %d\n", errno);
        return;
    }

    int enableReuse = 1;
    if (setsockopt(listeningSocket, SOL_SOCKET, SO_REUSEADDR, &enableReuse, sizeof(int)) < 0)
    {
        printf("ERROR: setsockopt() failed with error code: %d\n", errno);
        close(listeningSocket);
        return;
    }

    struct sockaddr_in serverAddress;
    bzero((char*)&serverAddress, sizeof(serverAddress));

    serverAddress.sin_family = AF_INET;
    serverAddress.sin_addr.s_addr = INADDR_ANY;
    serverAddress.sin_port = htons(TEST_PORT);
    if (bind(listeningSocket, (struct sockaddr *)&serverAddress, sizeof(serverAddress)) == -1)
    {
        printf("ERROR: Bind failed with error code: %d\n", errno);
        close(listeningSocket);
        return;
    }

    // start to listen for accepts
    if (listen(listeningSocket, 1) == -1)
    {
        printf("ERROR: listen() failed with error code: %d\n", errno);
        close(listeningSocket);
        return;
    }

    // send lets start
    if (safe_send(clientSocket, socketBuffer, 0) < 0) {
        return;
    }

    struct sockaddr_in clientAddress;
    socklen_t clientAddressLen = sizeof(clientAddress);

    memset(&clientAddress, 0, sizeof(clientAddress));
    clientAddressLen = sizeof(clientAddress);
    int sock = accept(listeningSocket, (struct sockaddr *) &clientAddress, &clientAddressLen);
    if (sock == -1) {
        printf("ERROR: Listen failed with error code: %d\n", errno);
        close(listeningSocket);
        close(sock);
        return;
    }

    close(listeningSocket);

	struct pollfd fds[2];
    fds[0].fd = clientSocket;
    fds[0].events = POLL_IN;
    fds[0].revents = 0;
    fds[1].fd = sock;
    fds[1].events = POLL_IN;
    fds[1].revents = 0;
	unsigned char running = 1;

    while (running) {
        poll(fds, 2, POLL_TIMEOUT);

        if (fds[0].revents & POLLIN) {
            running = 0;
        }

        if (fds[1].revents & POLLIN) {
        	int nBytes = recv(sock, data,(dataSize < TEST_BUFFER_SIZE) ? dataSize : TEST_BUFFER_SIZE, 0);

        	if (nBytes > 0 && dataSize > 0) {
            	data += nBytes;
            	dataSize -= nBytes;
        	}
        }
    }

    // close connection
    close(sock);
}

// ipv4_udp
void ipv4_udp_send_data(Options options, char* data, int dataSize)
{
    // open connection
    // Create socket
    int sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (sock == -1)
    {
        printf("ERROR: Could not create socket: %d\n", errno);
        return;
    }

    struct sockaddr_in serverAddress;
    memset(&serverAddress, 0, sizeof(serverAddress));
    serverAddress.sin_family = AF_INET;
    serverAddress.sin_port = htons(TEST_PORT);
    if (inet_pton(AF_INET, (const char*)options.ip, &serverAddress.sin_addr) <= 0)
    {
        printf("ERROR: inet_pton() failed");
        return;
    }

    int maxBufferSize = (UDP_BUFFER_MAX_SIZE < TEST_BUFFER_SIZE) ? UDP_BUFFER_MAX_SIZE : TEST_BUFFER_SIZE;

    // send data
    while (dataSize > 0)
    {
        int bytesSent = sendto(sock, data, (dataSize < maxBufferSize) ? dataSize : maxBufferSize, 0, (struct sockaddr *) &serverAddress, sizeof(serverAddress));

        if (bytesSent > 0) {
            data += bytesSent;
            dataSize -= bytesSent;
        }
    }

    // close connection
    close(sock);
}

void ipv4_udp_recv_data(int clientSocket, char* socketBuffer, char* socketBufferData, char* data, int dataSize)
{
    // open connection
    // Create socket
    int sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (sock == -1)
    {
        printf("ERROR: Could not create socket: %d\n", errno);
        return;
    }

    struct sockaddr_in serverAddress;
    bzero((char*)&serverAddress, sizeof(serverAddress));

    serverAddress.sin_family = AF_INET;
    serverAddress.sin_addr.s_addr = INADDR_ANY;
    serverAddress.sin_port = htons(TEST_PORT);
    if (bind(sock, (struct sockaddr *)&serverAddress, sizeof(serverAddress)) == -1)
    {
        printf("ERROR: Bind failed with error code: %d\n", errno);
        close(sock);
        return;
    }

    struct sockaddr_in clientAddress;
    socklen_t clientAddressLen = sizeof(clientAddress);
    memset((char *)&clientAddress, 0, sizeof(clientAddress));

    // send lets start
    if (safe_send(clientSocket, socketBuffer, 0) < 0) {
        return;
    }

    // wait and take the data
    struct pollfd fds[2];
    fds[0].fd = clientSocket;
    fds[0].events = POLL_IN;
    fds[0].revents = 0;
    fds[1].fd = sock;
    fds[1].events = POLL_IN;
    fds[1].revents = 0;
    unsigned char running = 1;

    while (running) {
        poll(fds, 2, POLL_TIMEOUT);

        if (fds[0].revents & POLLIN) {
            running = 0;
        }

        if (fds[1].revents & POLLIN) {
            int nBytes = recvfrom(sock, data, (dataSize < TEST_BUFFER_SIZE) ? dataSize : TEST_BUFFER_SIZE, 0, (struct sockaddr *) &clientAddress, &clientAddressLen);

            if (nBytes > 0 && dataSize > 0) {
                data += nBytes;
                dataSize -= nBytes;
            }
        }
    }

    // close connection
    close(sock);
}

// ipv6_tcp
void ipv6_tcp_send_data(Options options, char* data, int dataSize)
{
    // open connection
    // create a client socket
    int sock = socket(AF_INET6, SOCK_STREAM, 0);

    if (sock == -1)
    {
        printf("ERROR: Could not create socket: %d\n", errno);
        return;
    }

    // config and save the server address
    struct sockaddr_in6 serverAddress;

    char* serverIPv6 = malloc(strlen(options.ip) + 7 + 1);
    strcpy(serverIPv6, "::ffff:");
    strcpy(serverIPv6 + 7, options.ip);

    bzero(&serverAddress, sizeof(serverAddress));
    serverAddress.sin6_family = AF_INET6;
    serverAddress.sin6_port = htons(TEST_PORT);
    if (inet_pton(AF_INET6, (const char *)serverIPv6, &serverAddress.sin6_addr) <= 0)
    {
        printf("ERROR: inet_pton() failed\n");
        close(sock);
        return;
    }

    // connect to the server
    if (connect(sock, (struct sockaddr *)&serverAddress, sizeof(serverAddress)) == -1)
    {
        printf("ERROR: connect() failed with error code: %d\n", errno);
        close(sock);
        return;
    }

    // send data
    while (dataSize > 0)
    {
        int bytesSent = (int)send(sock, data, (dataSize < TEST_BUFFER_SIZE) ? dataSize : TEST_BUFFER_SIZE, 0);

        if (bytesSent > 0) {
            data += bytesSent;
            dataSize -= bytesSent;
        }
    }

    // close connection
    close(sock);
}

void ipv6_tcp_recv_data(int clientSocket, char* socketBuffer, char* socketBufferData, char* data, int dataSize)
{
    // open connection
    // create listening socket
    int listeningSocket = socket(AF_INET6, SOCK_STREAM, 0);

    if (listeningSocket == -1)
    {
        printf("ERROR: Could not create listening socket: %d\n", errno);
        return;
    }

    int enableReuse = 1;
    if (setsockopt(listeningSocket, SOL_SOCKET, SO_REUSEADDR, &enableReuse, sizeof(int)) < 0)
    {
        printf("ERROR: setsockopt() failed with error code: %d\n", errno);
        close(listeningSocket);
        return;
    }

    struct sockaddr_in6 serverAddress;
    bzero((char*)&serverAddress, sizeof(serverAddress));

    serverAddress.sin6_family = AF_INET6;
    serverAddress.sin6_addr = in6addr_any;
    serverAddress.sin6_port = htons(TEST_PORT);
    if (bind(listeningSocket, (struct sockaddr *)&serverAddress, sizeof(serverAddress)) == -1)
    {
        printf("ERROR: Bind failed with error code: %d\n", errno);
        close(listeningSocket);
        return;
    }

    // start to listen for accepts
    if (listen(listeningSocket, 1) == -1)
    {
        printf("ERROR: listen() failed with error code: %d\n", errno);
        close(listeningSocket);
        return;
    }

    // send lets start
    if (safe_send(clientSocket, socketBuffer, 0) < 0) {
        return;
    }

    struct sockaddr_in clientAddress;
    socklen_t clientAddressLen = sizeof(clientAddress);

    memset(&clientAddress, 0, sizeof(clientAddress));
    clientAddressLen = sizeof(clientAddress);
    int sock = accept(listeningSocket, (struct sockaddr *) &clientAddress, &clientAddressLen);
    if (sock == -1) {
        printf("ERROR: Listen failed with error code: %d\n", errno);
        close(listeningSocket);
        close(sock);
        return;
    }

    close(listeningSocket);

    // wait and take the data
    struct pollfd fds[2];
    fds[0].fd = clientSocket;
    fds[0].events = POLL_IN;
    fds[0].revents = 0;
    fds[1].fd = sock;
    fds[1].events = POLL_IN;
    fds[1].revents = 0;
    unsigned char running = 1;

    while (running) {
        poll(fds, 2, POLL_TIMEOUT);

        if (fds[0].revents & POLLIN) {
            running = 0;
        }

        if (fds[1].revents & POLLIN) {
            int nBytes = recv(sock, data,(dataSize < TEST_BUFFER_SIZE) ? dataSize : TEST_BUFFER_SIZE, 0);

            if (nBytes > 0 && dataSize > 0) {
                data += nBytes;
                dataSize -= nBytes;
            }
        }
    }

    // close connection
    close(sock);
}

// ipv6_udp
void ipv6_udp_send_data(Options options, char* data, int dataSize)
{
    // open connection
    // Create socket
    int sock = socket(AF_INET6, SOCK_DGRAM, IPPROTO_UDP);
    if (sock == -1)
    {
        printf("ERROR: Could not create socket: %d\n", errno);
        return;
    }

    // config and save the server address
    struct sockaddr_in6 serverAddress;

    char* serverIPv6 = malloc(strlen(options.ip) + 7 + 1);
    strcpy(serverIPv6, "::ffff:");
    strcpy(serverIPv6 + 7, options.ip);

    bzero(&serverAddress, sizeof(serverAddress));
    serverAddress.sin6_family = AF_INET6;
    serverAddress.sin6_port = htons(TEST_PORT);
    if (inet_pton(AF_INET6, (const char *)serverIPv6, &serverAddress.sin6_addr) <= 0)
    {
        printf("ERROR: inet_pton() failed\n");
        close(sock);
        return;
    }

    int maxBufferSize = (UDP_BUFFER_MAX_SIZE < TEST_BUFFER_SIZE) ? UDP_BUFFER_MAX_SIZE : TEST_BUFFER_SIZE;

    // send data
    while (dataSize > 0)
    {
        int bytesSent = sendto(sock, data, (dataSize < maxBufferSize) ? dataSize : maxBufferSize, 0, (struct sockaddr *) &serverAddress, sizeof(serverAddress));

        if (bytesSent > 0) {
            data += bytesSent;
            dataSize -= bytesSent;
        }
    }

    // close connection
    close(sock);
}

void ipv6_udp_recv_data(int clientSocket, char* socketBuffer, char* socketBufferData, char* data, int dataSize)
{
    // open connection
    // Create socket
    int sock = socket(AF_INET6, SOCK_DGRAM, IPPROTO_UDP);
    if (sock == -1)
    {
        printf("ERROR: Could not create socket: %d\n", errno);
        return;
    }

    struct sockaddr_in6 serverAddress;
    bzero((char*)&serverAddress, sizeof(serverAddress));

    serverAddress.sin6_family = AF_INET6;
    serverAddress.sin6_addr = in6addr_any;
    serverAddress.sin6_port = htons(TEST_PORT);
    if (bind(sock, (struct sockaddr *)&serverAddress, sizeof(serverAddress)) == -1)
    {
        printf("ERROR: Bind failed with error code: %d\n", errno);
        close(sock);
        return;
    }

    struct sockaddr_in6 clientAddress;
    socklen_t clientAddressLen = sizeof(clientAddress);
    memset((char *)&clientAddress, 0, sizeof(clientAddress));

    // send lets start
    if (safe_send(clientSocket, socketBuffer, 0) < 0) {
        return;
    }

    // wait and take the data
    struct pollfd fds[2];
    fds[0].fd = clientSocket;
    fds[0].events = POLL_IN;
    fds[0].revents = 0;
    fds[1].fd = sock;
    fds[1].events = POLL_IN;
    fds[1].revents = 0;
    unsigned char running = 1;

    while (running) {
        poll(fds, 2, POLL_TIMEOUT);

        if (fds[0].revents & POLLIN) {
            running = 0;
        }

        if (fds[1].revents & POLLIN) {
            int nBytes = recvfrom(sock, data, (dataSize < TEST_BUFFER_SIZE) ? dataSize : TEST_BUFFER_SIZE, 0, (struct sockaddr *) &clientAddress, &clientAddressLen);

            if (nBytes > 0 && dataSize > 0) {
                data += nBytes;
                dataSize -= nBytes;
            }
        }
    }

    // close connection
    close(sock);
}

// uds_stream
void uds_stream_send_data(Options options, char* data, int dataSize)
{
    // open connection
    // create a client socket
    int sock = socket(AF_UNIX, SOCK_STREAM, 0);

    if (sock == -1)
    {
        printf("ERROR: Could not create socket: %d\n", errno);
        return;
    }

    // config and save the server address
    struct sockaddr_un serverAddress;

    bzero(&serverAddress, sizeof(serverAddress));
    serverAddress.sun_family = AF_UNIX;
    strcpy(serverAddress.sun_path, TEST_UDS_STREAM_SOCKET_PATH);

    // connect to the server
    if (connect(sock, (struct sockaddr *)&serverAddress, sizeof(serverAddress)) == -1)
    {
        printf("ERROR: connect() failed with error code: %d\n", errno);
        close(sock);
        return;
    }

    // send data
    while (dataSize > 0)
    {
        int bytesSent = (int)send(sock, data, (dataSize < TEST_BUFFER_SIZE) ? dataSize : TEST_BUFFER_SIZE, 0);

        if (bytesSent > 0) {
            data += bytesSent;
            dataSize -= bytesSent;
        }
    }

    // close connection
    close(sock);
}

void uds_stream_recv_data(int clientSocket, char* socketBuffer, char* socketBufferData, char* data, int dataSize)
{
    // open connection
    remove(TEST_UDS_STREAM_SOCKET_PATH);

    // create listening socket
    int listeningSocket = socket(AF_UNIX, SOCK_STREAM, 0);

    if (listeningSocket == -1)
    {
        printf("ERROR: Could not create listening socket: %d\n", errno);
        return;
    }

    int enableReuse = 1;
    if (setsockopt(listeningSocket, SOL_SOCKET, SO_REUSEADDR, &enableReuse, sizeof(int)) < 0)
    {
        printf("ERROR: setsockopt() failed with error code: %d\n", errno);
        close(listeningSocket);
        return;
    }

    struct sockaddr_un serverAddress;

    bzero(&serverAddress, sizeof(serverAddress));
    serverAddress.sun_family = AF_UNIX;
    strcpy(serverAddress.sun_path, TEST_UDS_STREAM_SOCKET_PATH);

    if (bind(listeningSocket, (struct sockaddr *)&serverAddress, sizeof(serverAddress)) == -1)
    {
        printf("ERROR: Bind failed with error code: %d\n", errno);
        close(listeningSocket);
        return;
    }

    // start to listen for accepts
    if (listen(listeningSocket, 1) == -1)
    {
        printf("ERROR: listen() failed with error code: %d\n", errno);
        close(listeningSocket);
        return;
    }

    // send lets start
    if (safe_send(clientSocket, socketBuffer, 0) < 0) {
        return;
    }

    struct sockaddr_in clientAddress;
    socklen_t clientAddressLen = sizeof(clientAddress);

    memset(&clientAddress, 0, sizeof(clientAddress));
    clientAddressLen = sizeof(clientAddress);
    int sock = accept(listeningSocket, (struct sockaddr *) &clientAddress, &clientAddressLen);
    if (sock == -1) {
        printf("ERROR: Listen failed with error code: %d\n", errno);
        close(listeningSocket);
        close(sock);
        return;
    }

    close(listeningSocket);

    // wait and take the data
    struct pollfd fds[2];
    fds[0].fd = clientSocket;
    fds[0].events = POLL_IN;
    fds[0].revents = 0;
    fds[1].fd = sock;
    fds[1].events = POLL_IN;
    fds[1].revents = 0;
    unsigned char running = 1;

    while (running) {
        poll(fds, 2, POLL_TIMEOUT);

        if (fds[0].revents & POLLIN) {
            running = 0;
        }

        if (fds[1].revents & POLLIN) {
            int nBytes = recv(sock, data,(dataSize < TEST_BUFFER_SIZE) ? dataSize : TEST_BUFFER_SIZE, 0);

            if (nBytes > 0 && dataSize > 0) {
                data += nBytes;
                dataSize -= nBytes;
            }
        }
    }

    // close connection
    close(sock);
}

// uds_dgram
void uds_dgram_send_data(Options options, char* data, int dataSize)
{
    // open connection
    // Create socket
    int sock = socket(AF_UNIX, SOCK_DGRAM, 0);
    if (sock == -1)
    {
        printf("ERROR: Could not create socket: %d\n", errno);
        return;
    }

    // config and save the server address
    struct sockaddr_un serverAddress;

    bzero(&serverAddress, sizeof(serverAddress));
    serverAddress.sun_family = AF_UNIX;
    strcpy(serverAddress.sun_path, TEST_UDS_DGRAM_SOCKET_PATH);

    int maxBufferSize = (UDP_BUFFER_MAX_SIZE < TEST_BUFFER_SIZE) ? UDP_BUFFER_MAX_SIZE : TEST_BUFFER_SIZE;

    // send data
    while (dataSize > 0)
    {
        int bytesSent = sendto(sock, data, (dataSize < maxBufferSize) ? dataSize : maxBufferSize, 0, (struct sockaddr *) &serverAddress, sizeof(serverAddress));

        if (bytesSent > 0) {
            data += bytesSent;
            dataSize -= bytesSent;
        }
    }

    // close connection
    close(sock);
}

void uds_dgram_recv_data(int clientSocket, char* socketBuffer, char* socketBufferData, char* data, int dataSize)
{
    // open connection
    remove(TEST_UDS_DGRAM_SOCKET_PATH);

    // Create socket
    int sock = socket(AF_UNIX, SOCK_DGRAM, 0);
    if (sock == -1)
    {
        printf("ERROR: Could not create socket: %d\n", errno);
        return;
    }

    // config and save the server address
    struct sockaddr_un serverAddress;

    bzero(&serverAddress, sizeof(serverAddress));
    serverAddress.sun_family = AF_UNIX;
    strcpy(serverAddress.sun_path, TEST_UDS_DGRAM_SOCKET_PATH);

    if (bind(sock, (struct sockaddr *)&serverAddress, sizeof(serverAddress)) == -1)
    {
        printf("ERROR: Bind failed with error code: %d\n", errno);
        close(sock);
        return;
    }

    struct sockaddr_in clientAddress;
    socklen_t clientAddressLen = sizeof(clientAddress);
    memset((char *)&clientAddress, 0, sizeof(clientAddress));

    // send lets start
    if (safe_send(clientSocket, socketBuffer, 0) < 0) {
        return;
    }

    // wait and take the data
    struct pollfd fds[2];
    fds[0].fd = clientSocket;
    fds[0].events = POLL_IN;
    fds[0].revents = 0;
    fds[1].fd = sock;
    fds[1].events = POLL_IN;
    fds[1].revents = 0;
    unsigned char running = 1;

    while (running) {
        poll(fds, 2, POLL_TIMEOUT);

        if (fds[0].revents & POLLIN) {
            running = 0;
        }

        if (fds[1].revents & POLLIN) {
            int nBytes = recvfrom(sock, data, (dataSize < TEST_BUFFER_SIZE) ? dataSize : TEST_BUFFER_SIZE, 0, (struct sockaddr *) &clientAddress, &clientAddressLen);

            if (nBytes > 0 && dataSize > 0) {
                data += nBytes;
                dataSize -= nBytes;
            }
        }
    }

    // close connection
    close(sock);
}

// mmap
void mmap_send_data(Options options, char* data, int dataSize)
{
    // open connection
    char* filename = options.test_param;

    FILE* file = fopen(filename, "r+");

    if (file == NULL)
    {
        printf("ERROR: Open mapping file failed!\n");
        return;
    }

    char* ptr = mmap(NULL, dataSize + sizeof(int), PROT_READ | PROT_WRITE, MAP_SHARED, fileno(file), 0);

    if (ptr == MAP_FAILED)
    {
        printf("ERROR: Mapping failed!\n");
        return;
    }

    int* start = (int*)ptr;
    ptr += sizeof(int);

    int bytesSent = 0;

    // send data
    while (bytesSent < dataSize)
    {
        int bytesToSend = (dataSize - bytesSent < TEST_BUFFER_SIZE) ? dataSize - bytesSent : TEST_BUFFER_SIZE;

        memcpy(ptr, data, bytesToSend);

        data += bytesToSend;
        ptr += bytesToSend;

        bytesSent += bytesToSend;
        *start = bytesSent;
    }

    // close connection
    munmap(ptr, dataSize + sizeof(int));
    fclose(file);
}

void mmap_recv_data(int clientSocket, char* socketBuffer, char* socketBufferData, char* data, int dataSize)
{
    // open connection
    char* filename = socketBufferData + 1;

    remove(filename);

    FILE* file = fopen(filename, "w+");

    if (file == NULL)
    {
        printf("ERROR: Open mapping file failed!\n");
        return;
    }

    ftruncate(fileno(file), dataSize + sizeof(int));

    char* ptr = mmap(NULL, dataSize + sizeof(int), PROT_READ | PROT_WRITE, MAP_SHARED, fileno(file), 0);

    if (ptr == MAP_FAILED)
    {
        printf("ERROR: Mapping failed!\n");
        return;
    }

    int* start = (int*)ptr;
    *start = 0;

    ptr += sizeof(int);

    // send lets start
    if (safe_send(clientSocket, socketBuffer, 0) < 0) {
        return;
    }

    int bytesRecived = 0;

    // wait and take the data
    struct pollfd fds[2];
    fds[0].fd = clientSocket;
    fds[0].events = POLL_IN;
    fds[0].revents = 0;
    fds[1].fd = fileno(file);
    fds[1].events = POLL_IN;
    fds[1].revents = 0;
    unsigned char running = 1;

    while (running) {
        poll(fds, 2, POLL_TIMEOUT);

        if (fds[0].revents & POLLIN) {
            running = 0;
        }

        if (fds[1].revents & POLLIN) {
            int bytesToRecive = *start - bytesRecived;
            if (bytesToRecive > 0 && bytesRecived < dataSize)
            {
                memcpy(data, ptr, bytesToRecive);
                data += bytesToRecive;
                ptr += bytesToRecive;
                bytesRecived += bytesToRecive;
            }
        }
    }

    // close connection
    munmap((char*)start, dataSize + sizeof(int));
    fclose(file);
}

// pipe
void pipe_send_data(Options options, char* data, int dataSize)
{
    // open connection
    char* filename = options.test_param;

    FILE* file = fopen(filename, "w");

    if (file == NULL)
    {
        printf("ERROR: Cannot open the pipe!\n");
        return;
    }

    int fd = fileno(file);

    // send data
    while (0 < dataSize)
    {
        int bytesSent = write(fd, data, (dataSize < TEST_BUFFER_SIZE) ? dataSize : TEST_BUFFER_SIZE);

        if (bytesSent > 0) {
            data += bytesSent;
            dataSize -= bytesSent;
        }
    }

    // close connection
    fclose(file);
}

void pipe_recv_data(int clientSocket, char* socketBuffer, char* socketBufferData, char* data, int dataSize)
{
    // open connection
    char* filename = socketBufferData + 1;

    remove(filename);
    if (mkfifo(filename, 0666) == -1)
    {
        printf("ERROR: Make fifo failed!\n");
        return;
    }

    FILE* file = fopen(filename, "r+");

    if (file == NULL)
    {
        printf("ERROR: Cannot open the pipe!\n");
        return;
    }

    int fd = fileno(file);

    // send lets start
    if (safe_send(clientSocket, socketBuffer, 0) < 0) {
        return;
    }

    // wait and take the data
    struct pollfd fds[2];
    fds[0].fd = clientSocket;
    fds[0].events = POLL_IN;
    fds[0].revents = 0;
    fds[1].fd = fd;
    fds[1].events = POLL_IN;
    fds[1].revents = 0;
    unsigned char running = 1;

    while (running) {
        poll(fds, 2, POLL_TIMEOUT);

        if (fds[0].revents & POLLIN) {
            running = 0;
        }

        if (fds[1].revents & POLLIN) {
            int nBytes = read(fd, data, (dataSize < TEST_BUFFER_SIZE) ? dataSize : TEST_BUFFER_SIZE);

            if (nBytes > 0 && dataSize > 0) {
                data += nBytes;
                dataSize -= nBytes;
            }
        }
    }

    // close connection
    fclose(file);
}
