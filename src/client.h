// client.h

#ifndef CLIENT_H
#define CLIENT_H

#include "shared.h"

/**
 * run the client app
 *
 * @param options options
 * @return error code
 */
int client_app(Options options);

#endif
