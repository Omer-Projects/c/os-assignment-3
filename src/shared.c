#include "shared.h"

int safe_send(int sock, char *buffer, int dataSize)
{
    *(int*)buffer = dataSize;

    int bytesSent = (int)send(sock, buffer, sizeof(int) + dataSize, 0);

    if (-1 == bytesSent)
    {
        printf("ERROR: send() failed with error code: %d\n", errno);
        return -1;
    }
    else if (0 == bytesSent)
    {
        printf("ERROR: Peer has closed the TCP connection prior to send().\n");
        return -1;
    }
    else if (sizeof(int) + dataSize != bytesSent)
    {
        printf("ERROR: Sent only %d bytes from the required %d.\n", dataSize, bytesSent);
        return -1;
    }

    return 0;
}

int safe_recv(int sock, char* buffer, int dataSize)
{
    int nBytes = recv(sock, buffer,sizeof(int) + dataSize, 0);

    int length = *(int*)buffer;

    if (nBytes < sizeof(int) && nBytes < sizeof(int) + length) {
        return -1;
    }

    *(buffer + sizeof(int) + length) = '\0';

    return nBytes;
}

void print_result(Options* options, unsigned char command_type, long long start, long long end)
{
    char* handlersNames[8] = { "ipv4_tcp", "ipv4_udp", "ipv6_tcp", "ipv6_udp", "uds_dgram", "uds_stream", "mmap", "pipe" };

    if (end == 0)
    {
        // checksum error
        if (options->quiet)
        {
            printf("%s,checksum error\n", handlersNames[command_type - 2]);
        }
        else
        {
            printf("Result: %s,checksum error\n", handlersNames[command_type - 2]);
        }
    }
    else
    {
        if (options->quiet)
        {
            printf("%s,%lld\n", handlersNames[command_type - 2], end - start);
        }
        else
        {
            printf("Result: %s,%lld\n", handlersNames[command_type - 2], end - start);
        }
    }
}

char checksum(char* start, char* end)
{
    unsigned char ret = 0;

    while (start != end)
    {
        ret ^= *start;
        start++;
    }

    return (char)ret;
}

long long current_time_in_milliseconds()
{
    struct timeval tv;

    gettimeofday(&tv,NULL);
    return (((long long)tv.tv_sec)*1000)+(tv.tv_usec/1000);
}
